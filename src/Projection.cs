using MessageHandler;
using Yaus.Contracts.BlogPosts;
using Yaus.Framework.EventSourcing;

namespace ProjectBlogPostsToSearch
{
    public class Projection :
        IProjection<SearchEntry, BlogPostDrafted>,
        IProjection<SearchEntry, BlogPostPublished>,
        IProjection<SearchEntry, BlogPostUnpublished>,
        IProjection<SearchEntry, BlogPostUpdated>
    {
        public void Project(SearchEntry record, BlogPostDrafted msg)
        {
            record.Filter1 = msg.Details.Author;
            record.Reference1 = msg.Details.BlogId;
            record.User = msg.Details.AuthorId;
            record.Id = msg.BlogPostId;
            record.Title = msg.Details.Title;
            record.Description = msg.Details.Excerpt;
            record.Searchable1 = msg.Details.Content;
            record.Date1 = msg.Details.DatePosted;
            record.Date2 = msg.Details.DatePublished;
            record.Collection1 = msg.Details.Tags ?? new string[0];
            record.Filter2 = msg.Details.Url;
            record.State1 = msg.Details.State.ToString();
            record.Object = Json.Encode(msg.Details);
            record.Type = "BlogPost";
        }

        public void Project(SearchEntry record, BlogPostPublished msg)
        {
            record.Filter1 = msg.Details.Author;
            record.Reference1 = msg.Details.BlogId;
            record.User = msg.Details.AuthorId;
            record.Id = msg.BlogPostId;
            record.Title = msg.Details.Title;
            record.Description = msg.Details.Excerpt;
            record.Searchable1 = msg.Details.Content;
            record.Date1 = msg.Details.DatePosted;
            record.Date2 = msg.Details.DatePublished;
            record.Collection1 = msg.Details.Tags ?? new string[0];
            record.Filter2 = msg.Details.Url;
            record.State1 = msg.Details.State.ToString();
            record.Object = Json.Encode(msg.Details);
            record.Type = "BlogPost";
        }

        public void Project(SearchEntry record, BlogPostUnpublished msg)
        {
            record.Filter1 = msg.Details.Author;
            record.Reference1 = msg.Details.BlogId;
            record.User = msg.Details.AuthorId;
            record.Id = msg.BlogPostId;
            record.Title = msg.Details.Title;
            record.Description = msg.Details.Excerpt;
            record.Searchable1 = msg.Details.Content;
            record.Date1 = msg.Details.DatePosted;
            record.Date2 = msg.Details.DatePublished;
            record.Collection1 = msg.Details.Tags ?? new string[0];
            record.Filter2 = msg.Details.Url;
            record.State1 = msg.Details.State.ToString();
            record.Object = Json.Encode(msg.Details);
            record.Type = "BlogPost";
        }

        public void Project(SearchEntry record, BlogPostUpdated msg)
        {
            record.Filter1 = msg.Details.Author;
            record.Reference1 = msg.Details.BlogId;
            record.User = msg.Details.AuthorId;
            record.Id = msg.BlogPostId;
            record.Title = msg.Details.Title;
            record.Description = msg.Details.Excerpt;
            record.Searchable1 = msg.Details.Content;
            record.Date1 = msg.Details.DatePosted;
            record.Date2 = msg.Details.DatePublished;
            record.Collection1 = msg.Details.Tags ?? new string[0];
            record.Filter2 = msg.Details.Url;
            record.State1 = msg.Details.State.ToString();
            record.Object = Json.Encode(msg.Details);
            record.Type = "BlogPost";
        }
    }
}
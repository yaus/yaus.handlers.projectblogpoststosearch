using MessageHandler;
using Yaus.Framework.EventSourcing;

namespace ProjectBlogPostsToSearch
{
    public class AddToContainer : IInitialization
    {
        public void Init(IContainer container)
        {
            var source = container.Resolve<IConfigurationSource>();
            var config = source.GetConfiguration<ProjectBlogPostsToSearchConfig>();
            container.UseEventSourcing(config.ConnectionString)
                    .EnableProjections(config.ConnectionString);
        }
    }
}
﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using MessageHandler;
using Yaus.Framework.AzureSearch;
using Yaus.Framework.EventSourcing;
using Environment = MessageHandler.Environment;

namespace ProjectBlogPostsToSearch
{
    public class ProjectBlogPostsToSearchHandler :
        IStandingQuery<DynamicJsonObject>,
        IAction<DynamicJsonObject>
    {
        private readonly AzureSearchClient _searchclient;
        private string _sourceType;
        private string _index;
        private readonly MemoryCache _projections = new MemoryCache("Projections");
        private readonly IInvokeProjections _projectionInvoker;

        public ProjectBlogPostsToSearchHandler(IConfigurationSource source, IVariablesSource variables, ITemplatingEngine templating, IInvokeProjections projectionInvoker)
        {
            _projectionInvoker = projectionInvoker;

            dynamic channelVariables = variables.GetChannelVariables(Channel.Current());
            dynamic accountVariables = variables.GetAccountVariables(Account.Current());
            dynamic environmentVariables = variables.GetEnvironmentVariables(Environment.Current());

            var config = source.GetConfiguration<ProjectBlogPostsToSearchConfig>();

            var serviceName = templating.Apply(config.SearchServiceName, null, channelVariables, environmentVariables, accountVariables);
            var apiKey = templating.Apply(config.SearchServiceApiKey, null, channelVariables, environmentVariables, accountVariables);
            _sourceType = templating.Apply(config.SourceType, null, channelVariables, environmentVariables, accountVariables);
            _index = templating.Apply(config.SearchServiceIndex, null, channelVariables, environmentVariables, accountVariables);

            _searchclient = new AzureSearchClient(serviceName, apiKey);
        }

        public IObservable<DynamicJsonObject> Compose(IObservable<DynamicJsonObject> messages)
        {
            return from e in messages
                   where IsBlogPostEvent(e)
                   select e;
        }

        private bool IsBlogPostEvent(dynamic e)
        {
            string what = e.What;
            string blogPostId = e.BlogPostId;
            string sourceId = e.SourceId;
            string sourceType = e.SourceType;

            Trace.TraceInformation("Evaluating What:'{0}', BlogPostId:'{1}', SourceId:'{2}', SourceType:'{3}'", what, blogPostId, sourceId, sourceType);

            return e.What != null && e.BlogPostId != null && e.SourceId != null && e.SourceType == _sourceType;
        }

        public async Task Action(DynamicJsonObject t)
        {
            dynamic m = t;

            string blogPostId = m.BlogPostId;
            string what = m.What;

            Trace.TraceInformation("Received event from blog post '{0}'.", blogPostId);

            var eventType = GetType(what);
            if (eventType == null)
            {
                Trace.TraceInformation("Received event of unknown type '{0}' skipping", what);
                return;
            }

            var @event = Json.Decode(Json.Encode(t), eventType);
            if (@event == null)
            {
                Trace.TraceInformation("Could not deserialize json into claimed type '{0}'", what);
                return;
            }

            //todo, figure out a decent way for partition & leader election

            var lazy = new Lazy<SearchEntry>(() => _searchclient.Get<SearchEntry>(_index, blogPostId).Result ?? new SearchEntry() { Id = blogPostId });
            var cachedLazy = (Lazy<SearchEntry>)_projections.AddOrGetExisting(blogPostId, lazy, new CacheItemPolicy() { SlidingExpiration = TimeSpan.FromMinutes(1) });
            var blogPost = (cachedLazy ?? lazy).Value;

            Trace.TraceInformation("Retrieved blog post '{0}', going to apply projection.", blogPostId);

            _projectionInvoker.Invoke(blogPost, @event);

            Trace.TraceInformation("Projection applied to blog post '{0}', persisting.", blogPostId);
        }

        private static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.ExportedTypes.FirstOrDefault(t => t.Name == typeName);
                if (type != null)
                    return type;
            }
            return null;
        }

        public async Task Complete()
        {
            foreach (var pair in _projections)
            {
                var blogPost = ((Lazy<SearchEntry>)pair.Value).Value;
                await _searchclient.Upsert(_index, new object[] { blogPost });
            }
        }

    }
}
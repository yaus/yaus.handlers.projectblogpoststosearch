using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MessageHandler;
using Yaus.Framework.AzureSearch;
using Yaus.Framework.EventSourcing;
using Environment = MessageHandler.Environment;

namespace ProjectBlogPostsToSearch
{
    public class RestoreViewIfNeeded : IStartupTask
    {
        private readonly IBuildViews _viewBuilder;
        private readonly ITrackProjectionCodeChanges _projectionCode;
        private AzureSearchClient _searchclient;
        private string _sourceType;
        private string _index;

        public RestoreViewIfNeeded(IConfigurationSource source, IVariablesSource variables, ITemplatingEngine templating, IBuildViews viewBuilder, ITrackProjectionCodeChanges projectionCode)
        {
            _viewBuilder = viewBuilder;
            _projectionCode = projectionCode;

            dynamic channelVariables = variables.GetChannelVariables(Channel.Current());
            dynamic accountVariables = variables.GetAccountVariables(Account.Current());
            dynamic environmentVariables = variables.GetEnvironmentVariables(Environment.Current());

            var config = source.GetConfiguration<ProjectBlogPostsToSearchConfig>();

            var serviceName = templating.Apply(config.SearchServiceName, null, channelVariables, environmentVariables, accountVariables);
            var apiKey = templating.Apply(config.SearchServiceApiKey, null, channelVariables, environmentVariables, accountVariables);
            _sourceType = templating.Apply(config.SourceType, null, channelVariables, environmentVariables, accountVariables);
            _index = templating.Apply(config.SearchServiceIndex, null, channelVariables, environmentVariables, accountVariables);

            _searchclient = new AzureSearchClient(serviceName, apiKey);
        }

        public void Run()
        {
            var task = RunInternal();
            task.Wait();
        }

        private async Task RunInternal()
        {
            if (await _projectionCode.HasChanges(typeof(Projection)))
            {
                Trace.TraceInformation("Projection code has changes, restoring.");
                await CleanBlogPostsIndex();
                await PopulateBlogPostsIndex();

                await _projectionCode.RegisterChanges(typeof(Projection));

            }
        }

        private async Task PopulateBlogPostsIndex()
        {
            var blogPosts = await _viewBuilder.BuildAsync<SearchEntry>(_sourceType);

            var list = blogPosts.Values.ToList();
            if(list.Count > 0)
                await _searchclient.Upsert(_index, list.ToArray<object>());
        }

        private async Task CleanBlogPostsIndex()
        {
            var query = new QueryBuilder()
                .SearchMode(SearchMode.All)
                .Filter("type", "eq", "BlogPost");
           
            var results = await _searchclient.Search(_index, query.ToQueryString());
            foreach (var result in results.value)
            {
                await _searchclient.Delete(_index, "id", (string) result.id);
            }

        }

    }
}
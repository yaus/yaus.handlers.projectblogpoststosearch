namespace ProjectBlogPostsToSearch
{
    public enum BlogPostState
    {
        Draft,
        Published
    }
}